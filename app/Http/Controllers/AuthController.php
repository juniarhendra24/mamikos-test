<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\UserPoint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $token_validity = 24 * 60;

        $this->guard()->factory()->setTTL($token_validity);

        if(!$token = $this->guard()->attempt($validator->validated())){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'isPremium' => 'required',
            'name' => 'required|string|between:2,100',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        DB::beginTransaction();
        try {
            $user = User::create(array_merge(
                $validator->validated(),
                ['password' => bcrypt($request->password)]
            ));
            if($request->type == 'user'){
                if($request->isPremium == true){
                    $p = 40;
                }else{
                    $p = 20;
                }
                $point = UserPoint::create([
                    'point' => $p,
                    'user_id' => $user->id,
                    'activity' => 'register',
                    'type' => 'add'
                ]);
            }else{
                $point = true;
            }
            

            if($user && $point){
                $status = true;
            }else{
                $status = false;
            }
            
            DB::commit();
            return response()->json(['message' => 'User Created Successfully', 'status' => $status]);    
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User Created Unsuccessful'], $e);
        }
    }

    public function logout(){
        $this->guard()->logout();
        return response()->json(['message' => 'User Logged Out Successfully']);
    }

    public function profile(){
        $user = $this->guard()->user();
        $point_add =  DB::table('users_point')
                    ->select(DB::raw('SUM(point) as total_point'))
                    ->where(['user_id' => $user->id, 'type' => 'add'])
                    ->groupBy('user_id')
                    ->first();
        $point_reduce =  DB::table('users_point')
                    ->select(DB::raw('SUM(point) as total_point'))
                    ->where(['user_id' => $user->id, 'type' => 'reduce'])
                    ->groupBy('user_id')
                    ->first();
        $user->point_add = (!empty($point_add->total_point)) ? (int)$point_add->total_point : 0;
        $user->point_reduce = (!empty($point_reduce->total_point)) ? (int)$point_reduce->total_point : 0;
        $user->point_active = $user->point_add - $user->point_reduce;
        return response()->json($user);
    }

    public function refresh(){
        return $this->respondWithToken($this->guard()->refresh());
    }

    protected function respondWithToken($token){
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'token_validity' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    protected function guard(){
        return Auth::guard();
    }
}
