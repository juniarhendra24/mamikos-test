<?php

namespace App\Http\Controllers;

use App\Models\Kost;
use App\Models\Ask;
use App\Models\UserPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class KostController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->guard()->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $kosts = $this->user->kosts()->get(['id', 'name', 'type', 'description', 'owner']);
        return response()->json($kosts->toArray());

    }

    public function list(Request $request)
    {
        $dt = DB::table('kost AS a')
        ->leftJoin('users AS b', 'b.id', '=', 'a.owner')
        ->select('a.*', 'b.name AS owner_name', 'b.phone AS owner_phone')
        ->whereRaw('1 = 1')
        ->where(function($query) use ($request){
            $query->where('a.name', 'like', '%'.$request->search_key.'%')
                  ->orWhere('a.location', 'like', '%'.$request->search_key.'%')
                  ->orWhere('a.price', 'like', '%'.$request->search_key.'%');
        });
        if(!empty($request->sort_by) && !empty($request->sort_mode)){
            $sort = $request->sort_by.' '.$request->sort_mode;
        }else{
            $sort = 'id DESC';
        }
        $result = $dt->orderByRaw($sort)->get();
        return response()->json(["data" => $result]);
    }

    public function show($id)
    {
        $dt = DB::table('kost AS a')
        ->leftJoin('users AS b', 'b.id', '=', 'a.owner')
        ->select('a.*', 'b.name AS owner_name', 'b.phone AS owner_phone')
        ->whereRaw('1 = 1')
        ->where('a.id', '=', $id);
        $result = $dt->first();
        return response()->json($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'type' => 'required|string',
                'price' => 'required',
                'location' => 'required',
                'regulation_5person' => 'required',
                'regulation_curfew' => 'required',
                'regulation_24houraccess' => 'required',
                'since' => 'required|max:4'
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        $kost = new Kost();
        $kost->name = $request->name;
        $kost->type = $request->type;
        $kost->location = $request->location;
        $kost->price = $request->price;
        $kost->description = $request->description;
        $kost->regulation_5person = $request->regulation_5person;
        $kost->regulation_curfew = $request->regulation_curfew;
        $kost->regulation_24houraccess = $request->regulation_24houraccess;
        $kost->since = $request->since;
        $kost->manager = $request->manager;
        $kost->manager_phone = $request->manager_phone;
        $kost->other_notes = $request->other_notes;

        if($this->user->type != 'owner'){
            return response()->json(
                [
                    'status' => false,
                    'message' => 'You are not owner, you cannot add kos'
                ]
            );
        }

        if ($this->user->kosts()->save($kost)) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data saved successfully',
                    'data_inserted'   => $kost,
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Data failed to save',
                    'data_inserted'   => null,
                ]
            );
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'type' => 'required|string',
                'price' => 'required',
                'location' => 'required',
                'regulation_5person' => 'required',
                'regulation_curfew' => 'required',
                'regulation_24houraccess' => 'required',
                'since' => 'required|max:4'
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        if($this->user->type != 'owner'){
            return response()->json(
                [
                    'status' => false,
                    'message' => 'You are not owner, you cannot update kos'
                ]
            );
        }

        $kost = new Kost();
        $kost->name = $request->name;
        $kost->type = $request->type;
        $kost->location = $request->location;
        $kost->price = $request->price;
        $kost->description = $request->description;
        $kost->regulation_5person = $request->regulation_5person;
        $kost->regulation_curfew = $request->regulation_curfew;
        $kost->regulation_24houraccess = $request->regulation_24houraccess;
        $kost->since = $request->since;
        $kost->manager = $request->manager;
        $kost->manager_phone = $request->manager_phone;
        $kost->other_notes = $request->other_notes;
        $update = Kost::where('id', $request->id)->update($kost->toArray());
        if ($update) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data updated successfully',
                    'data_updated'   => $kost
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Data failed to update',
                    'data_updated'   => null
                ]
            );
        }

    }

    public function ask(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'kost_id' => 'required',
                'question' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }
        DB::beginTransaction();
        try {
            $ask = new Ask();
            $ask->ask_kost = $request->kost_id;
            $ask->ask_question = $request->question;

            $point = UserPoint::create([
                'point' => '5',
                'user_id' => $this->user->id,
                'activity' => 'ask_room_availability',
                'type' => 'reduce'
            ]);
            if($this->user->asks()->save($ask)){
                $status = true;
                $message = 'Data saved successfully';
            }else{
                $status = false;
                $message = 'Data failed to save';
            }
            
            DB::commit();
            return response()->json(['message' => $message, 'status' => $status]);    
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'error DB Transaction'], $e);
        }
    }

    public function delete($id){
        if($this->user->type != 'owner'){
            return response()->json(
                [
                    'status' => false,
                    'message' => 'You are not owner, you cannot delete kos'
                ]
            );
        }

        $kost = Kost::find($id);
        if(empty($kost)){
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data Not Found'
                ]
            );
        }

        if($this->user->id != $kost->owner){
            return response()->json(
                [
                    'status' => false,
                    'message' => 'You are not the owner of this kos, you cannot delete kos'
                ]
            );
        }

        if (Kost::where('id', $id)->delete()) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data deleted successfully'
                ]
            );
        } else {
            return response()->json(
                [
                    'status'  => false,
                    'message' => 'Data failed to delete'
                ]
            );
        }
    }

    protected function guard(){
        return Auth::guard();
    }
}
