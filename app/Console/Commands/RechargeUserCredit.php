<?php

namespace App\Console\Commands;


use App\Models\UserPoint;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Console\Command;

class RechargeUserCredit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recharge:usercredit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For recharge user credit every month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::where('type', 'user')
               ->get();
        foreach($user as $value){
            $value->point = ($value->isPremium) ? '40' : '20'; 
            UserPoint::create([
                'point' => $value->point,
                'user_id' => $value->id,
                'activity' => 'recharge credit monthly',
                'type' => 'add'
            ]);
        }
        info("recharged credit monthly");
        return 0;
    }
}
