<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kost extends Model
{

    protected $table = 'kost';

    protected $fillable = [
        'name',
        'type',
        'location',
        'price',
        'owner',
        'description',
        'regulation_5person',
        'regulation_curfew',
        'regulation_24houraccess',
        'since',
        'manager',
        'manager_phone',
        'other_notes'
    ];
}
