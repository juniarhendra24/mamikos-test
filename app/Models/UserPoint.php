<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPoint extends Model
{

    protected $table = 'users_point';

    protected $fillable = [
        'point',
        'user_id',
        'activity',
        'type'
    ];
}
