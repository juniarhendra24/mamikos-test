<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ask extends Model
{

    protected $table = 'ask';

    protected $fillable = [
        'ask_user',
        'ask_kost',
        'ask_question'
    ];
}
