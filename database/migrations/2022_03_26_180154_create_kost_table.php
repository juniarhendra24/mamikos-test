<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kost', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('type', ['putra','putri', 'campur'])->nullable(false)->default('putra');
            $table->unsignedBigInteger('owner');
            $table->foreign('owner')->references('id')->on('users')->onDelete('cascade');
            $table->string('location')->nullable(true);
            $table->string('price')->nullable(true);
            $table->text('description')->nullable(true);
            $table->boolean('regulation_5person')->default(false);
            $table->boolean('regulation_curfew')->default(false);
            $table->boolean('regulation_24houraccess')->default(false);
            $table->year('since');
            $table->string('manager')->nullable(true);
            $table->string('manager_phone')->nullable(true);
            $table->text('other_notes')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kost');
    }
}
