<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ask', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ask_user');
            $table->foreign('ask_user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('ask_kost');
            $table->foreign('ask_kost')->references('id')->on('kost')->onDelete('cascade');
            $table->text('ask_question')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ask');
    }
}
